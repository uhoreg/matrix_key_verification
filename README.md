Matrix Key Verification Test
============================

Hubert Chathi <hubert@uhoreg.ca>

This is an demo implementation of my
[proposed "In-person verification" key verification method](https://cryptpad.fr/pad/#/1/edit/0eqaXl2wZhiq5s5Ty-wzcg/QL4+6V9poZCmbZI0ispNMFbU/).

The demo starts by prompting for a username, password, and homeserver.  After
clicking the "Next" button, it will log in two sessions with the same user, and
demonstrate how the two sessions verify each others' keys.  One session is
"Alice", and the other session is "Bob".  It will display a log of what
happens, with Alice's actions (or Alice's device's actions) left-justified and
with a green background, and Bob's actions (or Bob's device's actions)
right-justified with a blue background.  When a step is done, clicking the
"Next" button will go to the next step.  When the short authentication string
is displayed, a select element will be displayed, which will allow you to
change the type of string displayed.

The source code may be more interesting than the demo page, and contains
comments on changes that would need to be made in order to create a proper
implementation.

**Disclaimer:** I don't make any claims regarding the security of this
implementation, especially as this is my first attempt at using the Web Crypto
API.  This implementation is merely a demonstration of the proposal, and is
only intended to show how the steps might work.

Credits
-------

Emoji images from [Emojitwo](https://emojitwo.github.io/) (based on
[Emojione](https://www.emojione.com/)), licensed under a Creative Commons
License ([CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)).

Uses a
[word list provided by the EFF](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases).

Uses [matrix-js-sdk](https://github.com/matrix-org/matrix-js-sdk) and
[olm](https://git.matrix.org/git/olm), both released under the Apache
License 2.0.

License
-------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute
this software, either in source code form or as a compiled binary, for any
purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of
this software dedicate any and all copyright interest in the software to the
public domain. We make this dedication for the benefit of the public at
large and to the detriment of our heirs and successors. We intend this
dedication to be an overt act of relinquishment in perpetuity of all present
and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
