/* eslint-env browser */
/* global matrixcs effShortWordlist2 emojilist */

/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 * this software, either in source code form or as a compiled binary, for any
 * purpose, commercial or non-commercial, and by any means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors of
 * this software dedicate any and all copyright interest in the software to the
 * public domain. We make this dedication for the benefit of the public at
 * large and to the detriment of our heirs and successors. We intend this
 * dedication to be an overt act of relinquishment in perpetuity of all present
 * and future rights to this software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

(function () {
    /* Matrix key verification test.
     * The interesting parts start where the "steps" variable is defined (search for "var steps").
     */
    "use strict";

    var btn_next = document.getElementById("next");
    var div_log = document.getElementById("log");
    var div_sas_type_selection = document.getElementById("sas_type_selection");

    /***************************************************************************
     * Misc utility stuff
     **************************************************************************/

    /** dummy in-memory storage that provites the same interface as localStorage
     * (used for the Matrix client's sessionstore)
     */
    function DummyStorage() {
        this._keys = [];
        this._map = {};
        Object.defineProperty(this, "length", {
            get: function () {
                return this._keys.length;
            }.bind(this)
        });
    }
    DummyStorage.prototype.key = function key (n) {
        return this._keys[n];
    };
    DummyStorage.prototype.getItem = function getItem(key) {
        if ("prop_" + key in this._map) {
            return this._map["prop_" + key];
        } else {
            return null;
        }
    };
    DummyStorage.prototype.setItem = function setItem(key, value) {
        this._map["prop_" + key] = value;
        if (!this._keys.includes(value)) {
            this._keys.push(value);
        }
    };
    DummyStorage.prototype.removeItem = function removeItem(key) {
        delete this._map["prop_" + key];
        this._keys = this._keys.filter(function (k) {
            return key !== k;
        });
    };
    DummyStorage.prototype.clear = function clear() {
        this._keys = [];
        this._map = {};
    };

    function arrayBufferToHex(arrayBuffer) {
        var rv = "";
        var uint8Array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < uint8Array.length; i++) {
            var entry_hex = uint8Array[i].toString(16);
            if (entry_hex.length < 2)
            {
                entry_hex = "0" + entry_hex;
            }
            rv += entry_hex;
        }
        return rv;
    }

    function hexToArrayBuffer(hex) {
        var uint8Array = new Uint8Array(hex.length / 2);
        for (var i = 0; i < hex.length; i += 2) {
            uint8Array[i / 2] = parseInt(hex[i] + hex[i + 1], 16);
        }
        return uint8Array.buffer;
    }

    /** convert a string (with just ASCII characters) to an array buffer
     * FIXME: since this is only done on base64 strings, it would be better to
     * base64-decode the string to create an array buffer
     */
    function stringToArrayBuffer(str) {
        var uint8Array = new Uint8Array(str.length);
        for (var i = 0; i < str.length; i++) {
            uint8Array[i] = str.charCodeAt(i);
        }
        return uint8Array.buffer;
    }

    /***************************************************************************
     * Logging stuff
     **************************************************************************/

    /** utility function for logging
     */
    function makeContent(content) {
        if (!(content instanceof Node)) {
            return document.createTextNode(content.toString());
        } else {
            return content;
        }
    }

    /** add an HTML element to the log
     */
    function addToLog(logline) {
        div_log.appendChild(logline);
        div_log.scrollTop = div_log.scrollHeight;
    }

    /** log an action taken by one of the people
     */
    function log(actor, content) {
        var logline = document.createElement("div");
        logline.appendChild(makeContent(content));
        logline.setAttribute("class", actor);
        addToLog(logline);
    }

    var has_error = false;
    /** log an error that has occurred
     */
    function error(content) {
        var logline = document.createElement("div");
        logline.appendChild(makeContent(content));
        logline.setAttribute("class", "error");
        addToLog(logline);

        btn_next.textContent = "Error!";
        btn_next.disabled = true;
        has_error = true;
    }

    /***************************************************************************
     * Dramatis personae
     **************************************************************************/

    var alice = {
        _bob: {}, // information about Bob that Alice knows
        tellBob: function (key, value) {
            bob.rememberAlices(key, value); // eslint-disable-line no-use-before-define
        },
        recallBobs: function (key) {
            return alice._bob[key];
        },
        rememberBobs: function (key, value) {
            alice._bob[key] = value;
        },
        sendToBobsDevice: function (type, content) {
            var contentMap = {};
            contentMap[alice.recallBobs("user_id")] = {};
            contentMap[alice.recallBobs("user_id")][alice.recallBobs("device_id")] = content;
            alice.client.sendToDevice(type, contentMap);
        },
        log: log.bind(undefined, "alice")
    };
    var bob = {
        _alice: {}, // information about Alice that Bob knows
        tellAlice: function (key, value) {
            alice.rememberBobs(key, value);
        },
        recallAlices: function (key) {
            return bob._alice[key];
        },
        rememberAlices: function (key, value) {
            bob._alice[key] = value;
        },
        sendToAlicesDevice: function (type, content) {
            var contentMap = {};
            contentMap[bob.recallAlices("user_id")] = {};
            contentMap[bob.recallAlices("user_id")][bob.recallAlices("device_id")] = content;
            bob.client.sendToDevice(type, contentMap);
        },
        log: log.bind(undefined, "bob")
    };


    /***************************************************************************
     * Key verification steps
     **************************************************************************/

    /** resets the UI when a step in the key verification is done
     */
    function done() {
        if (has_error) {
            return;
        } if (curr_step >= steps.length) { // eslint-disable-line no-use-before-define
            btn_next.textContent = "Done";
        } else {
            btn_next.textContent = "Next >";
            btn_next.disabled = false;
        }

        var logline = document.createElement("div");
        logline.textContent = "Step done";
        logline.setAttribute("class", "done");
        addToLog(logline);
    }

    /** create an HTML element containing different versions of the short
     * authentication string based on the output from deriveKey.  Each
     * different version is placed in a <span> with a specific class name, so
     * that it can be shown/hidden.
     * FIXME: allow users to specify a length?  The current strings provide
     * between 40-48 bits of information.
     */
    function arrayBufferToSAS(arrayBuffer) {
        // FIXME: hash the buffer first, so that the SAS doesn't leak the
        // secret bits (though that would make this function async)

        var sasElement = document.createElement("span");
        sasElement.setAttribute("class", "sas");
        var i;

        // FIXME: this method of calculating a words/emoji SAS is kind of
        // cheating and only really works if your set of words/emoji list is a
        // power of 2.  It's fine for a demo, though.
        // FIXME: This demo just uses the first 1024 words from the EFF's second
        // short wordlist.  It's probably better to use the full long wordlist
        // instead (or at least most of it), since that would result in a shorter
        // SAS or more bits of information
        var wordsElement = document.createElement("span");
        wordsElement.setAttribute("class", "sas_words");
        var words = [];
        var int16Array = new Uint16Array(arrayBuffer);
        for (i = 0; i < 4; i++)
        {
            words.push(effShortWordlist2[int16Array[i] & 1023]);
        }
        wordsElement.textContent = words.join(" ");
        sasElement.appendChild(wordsElement);

        // FIXME: This demo just uses a set of 64 emoji.  It would be better if
        // it could use a larger set, provided that the emoji are distinct
        // and clear enough for users to use.
        var emojiElement = document.createElement("span");
        emojiElement.setAttribute("class", "sas_emoji");
        var int8Array = new Uint8Array(arrayBuffer);
        for (i = 0; i < 8; i++)
        {
            var imgElement = document.createElement("img");
            var emoji = emojilist[int8Array[i] & 63];
            imgElement.setAttribute("src", "images/emojitwo/" + emoji.codePointAt(0).toString(16) + ".svg");
            imgElement.setAttribute("alt", emoji);
            // FIXME: add title attribute
            imgElement.setAttribute("width", 48);
            imgElement.setAttribute("height", 48);
            emojiElement.appendChild(imgElement);
        }
        sasElement.appendChild(emojiElement);

        var hexElement = document.createElement("span");
        hexElement.setAttribute("class", "sas_hex");
        var hex = arrayBufferToHex(arrayBuffer);
        hexElement.textContent = hex.substr(0, 4) + " " + hex.substr(4, 4) + " " + hex.substr(8, 4);
        sasElement.appendChild(hexElement);

        return sasElement;
    }

    /** list of step in the key verification process.
     * each entry is an array where the first element is a description, and the
     * second element is a function that implements that step
     */
    var steps = [
        [
            "Alice and Bob are logged in, meet in person, and tell each other the device that they have.",
            function () {
                document.getElementById("login").style.display = "none";
                var hs = document.getElementById("homeserver").value;
                var userid = document.getElementById("userid").value;
                var password = document.getElementById("password").value;

                alice.log("Alice logs in...");
                matrixcs.createClient(hs).loginWithPassword(userid, password)
                    .then(function (data) {
                        alice.client = matrixcs.createClient({
                            baseUrl: hs,
                            accessToken: data.access_token,
                            userId: data.user_id,
                            deviceId: data.device_id,
                            sessionStore: new matrixcs.WebStorageSessionStore(new DummyStorage()),
                            cryptoStore: new matrixcs.MemoryCryptoStore()
                        });
                        alice.user_id = data.user_id;
                        alice.device_id = data.device_id;

                        alice.client.on("toDeviceEvent", handleAliceEvent); // eslint-disable-line no-use-before-define

                        return alice.client.initCrypto();
                    })
                    .then(function () {
                        alice.client.startClient();

                        alice.tellBob("user_id", alice.user_id);
                        alice.tellBob("device_id", alice.device_id);
                        alice.log("Alice has logged in and tells Bob her device ID (" + alice.device_id + ")");
                        if (bob.client) {
                            done();
                        }
                    })
                    .catch(error);

                bob.log("Bob logs in...");
                matrixcs.createClient(hs).loginWithPassword(userid, password)
                    .then(function (data) {
                        bob.client = matrixcs.createClient({
                            baseUrl: hs,
                            accessToken: data.access_token,
                            userId: data.user_id,
                            deviceId: data.device_id,
                            sessionStore: new matrixcs.WebStorageSessionStore(new DummyStorage()),
                            cryptoStore: new matrixcs.MemoryCryptoStore()
                        });
                        bob.user_id = data.user_id;
                        bob.device_id = data.device_id;

                        bob.client.on("toDeviceEvent", handleBobEvent); // eslint-disable-line no-use-before-define

                        return bob.client.initCrypto();
                    })
                    .then(function () {
                        bob.client.startClient();

                        bob.tellAlice("user_id", bob.user_id);
                        bob.tellAlice("device_id", bob.device_id);
                        bob.log("Bob has logged in and tells Alice his device ID (" + bob.device_id + ")");
                        if (alice.client) {
                            done();
                        }
                    })
                    .catch(error);
            }
        ],
        [
            // FIXME: steps should have some sort of timeout (i.e. after a
            // device sends a message, it expects a reply from the other device
            // by a certain time)
            "Alice initiates key verification",
            function () {
                alice.log("Alice clicks on the \"Verify\" button next to Bob's device");
                alice.log("Alice's device sends a key verification request to Bob's device.");
                var content = {
                    from_device: alice.device_id,
                    // to identify this key verification flow (not sure if it's
                    // actually necessary, since any pair of devices should
                    // only have one key verification between them at a given
                    // time)
                    transaction_id: "some_random_string",
                    // secret-sharing algorithms that Alice understands, along
                    // with possible parameters
                    secret_algorithms: [
                        {
                            name: "ECDH",
                            namedCurve: ["P-256", "P-384", "P-521"],
                            format: ["raw", "spki"]
                        }
                    ],
                    // hashes that Alice understands
                    hashes: ["SHA-1", "SHA-256", "SHA-512"],
                    // MACs that Alice understands
                    message_authentication_codes: ["HMAC-SHA1", "HMAC-SHA256", "HMAC-SHA512"],
                    // SAS types that Alice understands
                    // FIXME: SAS names should probably be more descriptive,
                    // like "eff_short_wordlist_1024"
                    short_authentication_string: [
                        "words", "emoji", "hex"
                    ]
                };
                alice.sendToBobsDevice("ca.uhoreg.key_verification.start", content);
                // this step continued in Bob's event handler
            }
        ],
        [
            "Bob accepts the key verification and begins the secret sharing",
            function () {
                bob.log("Bob accepts the key verification");
                bob.log("Bob's device generates its key for secret sharing");
                // FIXME: select something based on Alice's request
                crypto.subtle.generateKey(
                    {
                        name: "ECDH",
                        namedCurve: "P-256"
                    },
                    true,
                    ["deriveKey", "deriveBits"]
                )
                    .then(function (key) {
                        bob.ecdh_key = key;
                        return crypto.subtle.exportKey("raw", key.publicKey);
                    })
                    .then(function (keydata) {
                        // FIXME: base64 is probably better than hex for real usage
                        bob.ecdh_key_encoded = arrayBufferToHex(keydata); // remember this for later
                        return crypto.subtle.digest({ name: "SHA-256" }, keydata);
                    })
                    .then(function (hash) {
                        var hash_hex = arrayBufferToHex(hash);
                        bob.log("Bob's device sends the chosen parameters and hash commitment to Alice's device.");
                        // NOTE: the hash commitment makes sure that a MITM
                        // can't try to brute-force the Diffie-Hellman exchange
                        var content = {
                            transaction_id: "some_random_string",
                            secret_algorithm: {
                                name: "ECDH",
                                namedCurve: "P-256",
                                format: "raw"
                            },
                            hash: "SHA-256",
                            message_authentication_code: "HMAC-SHA512",
                            short_authentication_string: [ // SAS types that Bob understands
                                "words", "emoji", "hex"
                            ],
                            commitment: hash_hex
                        };
                        bob.sendToAlicesDevice("ca.uhoreg.key_verification.accept", content);
                        // this step continued in Alice's event handler
                    })
                    .catch(error);
            }
        ],
        [
            "Alice's device continues the secret sharing",
            function () {
                alice.log("Alice's device generates its key for secret sharing");
                // FIXME: use parameters sent from Bob's response
                crypto.subtle.generateKey(
                    {
                        name: "ECDH",
                        namedCurve: "P-256"
                    },
                    true,
                    ["deriveKey", "deriveBits"]
                )
                    .then(function (key) {
                        alice.ecdh_key = key;
                        return crypto.subtle.exportKey("raw", key.publicKey);
                    })
                    .then(function (keydata) {
                        var keydata_hex = arrayBufferToHex(keydata);
                        alice.log("Alice's device sends its key to Bob's device");
                        var content = {
                            transaction_id: "some_random_string",
                            key: keydata_hex
                        };
                        alice.sendToBobsDevice("ca.uhoreg.key_verification.key", content);
                        // this step continued in Bob's event handler
                    })
                    .catch(error);
            }
        ],
        [
            "Bob's device continues the secret sharing",
            function () {
                bob.log("Bob's device sends its previously-generated key to Alice's device");
                var content = {
                    transaction_id: "some_random_string",
                    key: bob.ecdh_key_encoded
                };
                bob.sendToAlicesDevice("ca.uhoreg.key_verification.key", content);
                // this step continued in Alice's event handler
            }
        ],
        [
            "Alice and Bob's devices compute the shared secret and short authentication string",
            function () {
                var alice_done = false;
                var bob_done = false;
                alice.log("Alice's device uses the keys to compute the shared secret");
                crypto.subtle.deriveKey(
                    {
                        name: "ECDH",
                        namedCurve: "P-256",
                        public: alice.recallBobs("ecdh_publickey")
                    },
                    alice.ecdh_key.privateKey,
                    {
                        name: "HMAC",
                        hash: {name: "SHA-256"},
                        length: 256
                    },
                    false,
                    ["sign", "verify"]
                )
                    .then(function (key) {
                        alice.hmac_key = key;
                        return crypto.subtle.deriveBits(
                            {
                                name: "ECDH",
                                namedCurve: "P-256",
                                public: alice.recallBobs("ecdh_publickey")
                            },
                            alice.ecdh_key.privateKey,
                            256
                        );
                    })
                    .then(function (bits) {
                        alice.log("Alice's device computes the short authentication string and displays it: ");
                        alice.log(arrayBufferToSAS(bits));
                        alice.sas = arrayBufferToHex(bits).substr(12);

                        div_sas_type_selection.style.display = "block";
                        var select_sas = document.getElementById("sas_type");
                        select_sas.addEventListener("change", function (event) {
                            document.getElementsByTagName("body")[0].setAttribute("class", "sas_" + select_sas.value);
                        });

                        if (bob_done) {
                            done();
                        } else {
                            alice_done = true;
                        }
                    })
                    .catch(function (err) {
                        error(err);
                    });
                bob.log("Bob's device uses the keys to compute the shared secret");
                crypto.subtle.deriveKey(
                    {
                        name: "ECDH",
                        namedCurve: "P-256",
                        public: bob.recallAlices("ecdh_publickey")
                    },
                    bob.ecdh_key.privateKey,
                    {
                        name: "HMAC",
                        hash: {name: "SHA-256"},
                        length: 256
                    },
                    false,
                    ["sign", "verify"]
                )
                    .then(function (key) {
                        bob.hmac_key = key;
                        return crypto.subtle.deriveBits(
                            {
                                name: "ECDH",
                                namedCurve: "P-256",
                                public: bob.recallAlices("ecdh_publickey")
                            },
                            bob.ecdh_key.privateKey,
                            256
                        );
                    })
                    .then(function (bits) {
                        bob.log("Bob's device computes the short authentication string and displays it: ");
                        bob.log(arrayBufferToSAS(bits));
                        bob.sas = arrayBufferToHex(bits).substr(12);
                        if (alice_done) {
                            done();
                        } else {
                            bob_done = true;
                        }
                    })
                    .catch(error);
            }
        ],
        [
            "Alice and Bob verify the displayed short authentication strings",
            function () {
                div_sas_type_selection.style.display = "none";
                alice.log("Alice ...");
                bob.log("and Bob compare the short authentication strings");
                alice.tellBob("sas", alice.sas);
                bob.tellAlice("sas", bob.sas);
                if (alice.sas !== alice.recallBobs("sas")) {
                    error("Alice says the SAS doesn't match");
                } else {
                    alice.log("Alice tells her device that the SAS matches");
                }
                if (bob.sas !== bob.recallAlices("sas")) {
                    error("Bob says the SAS doesn't match");
                } else {
                    bob.log("Bob tells his device that the SAS matches");
                }
                done();
            }
        ],
        [
            "Alice and Bob's devices verify their device keys",
            function () {
                alice.log("Alice's devices sends an HMAC of her device key (using the shared secret as HMAC key) to Bob's device");
                crypto.subtle.sign(
                    {
                        name: "HMAC"
                    },
                    alice.hmac_key,
                    // FIXME: base64-decode and then convert to ArrayBuffer instead
                    stringToArrayBuffer(alice.client.getDeviceEd25519Key())
                )
                    .then(function (sig) {
                        var content = {
                            transaction_id: "some_random_string",
                            mac: arrayBufferToHex(sig)
                        };
                        alice.sendToBobsDevice("ca.uhoreg.key_verification.mac", content);
                        // this part of this step is handled in Bob's event handler
                    })
                    .catch(error);
                bob.log("Bob's devices sends an HMAC of his device key (using the shared secret as HMAC key) to Alice's device");
                crypto.subtle.sign(
                    {
                        name: "HMAC"
                    },
                    bob.hmac_key,
                    // FIXME: base64-decode and then convert to ArrayBuffer instead
                    stringToArrayBuffer(bob.client.getDeviceEd25519Key())
                )
                    .then(function (sig) {
                        var content = {
                            transaction_id: "some_random_string",
                            mac: arrayBufferToHex(sig)
                        };
                        bob.sendToAlicesDevice("ca.uhoreg.key_verification.mac", content);
                        // this part of this step is handled in Alice's event handler
                    })
                    .catch(error);
            }
        ]
    ];
    var curr_step = 0;

    /** Alice's to_device event handler
     */
    function handleAliceEvent(event) {
        console.log("Alice received an event", event); // eslint-disable-line no-console
        switch (event.event.type) {
        case "ca.uhoreg.key_verification.accept":
            // from Step 2
            alice.rememberBobs("commitment", event.event.content.commitment);
            alice.log("Alice's device receives the key verification acceptance and stores hash commitment");
            // FIXME: make sure the parameters sent by Bob are acceptable
            done();
            break;
        case "ca.uhoreg.key_verification.key":
            // from Step 4
            alice.log("Alice's device receives the key");
            alice.log("Alice's device hashes the key and compares with Bob's hash commitment");
            var keydata = hexToArrayBuffer(event.event.content.key);
            crypto.subtle.digest({ name: "SHA-256" }, keydata)
                .then(function (hash) {
                    var hash_hex = arrayBufferToHex(hash);
                    if (hash_hex !== alice.recallBobs("commitment"))
                    {
                        throw "Bob's key does not match his hash commitment";
                    }
                    else
                    {
                        alice.log("Bob's key matches his hash commitment");
                    }

                    return crypto.subtle.importKey(
                        "raw",
                        hexToArrayBuffer(event.event.content.key),
                        {
                            name: "ECDH",
                            namedCurve: "P-256"
                        },
                        true,
                        []
                    );
                })
                .then(function (key) {
                    alice.rememberBobs("ecdh_publickey", key);
                    done();
                })
                .catch(error);
            break;
        case "ca.uhoreg.key_verification.mac":
            // from Step 7
            alice.log("Alice's device receives the HMAC of Bob's key");
            alice.log("Alice's device checks Bob's device key using the HMAC");
            // FIXME: in a real client, you would probaby use alice.client.getStoredDevice
            alice.client.downloadKeys([alice.recallBobs("user_id")])
                .then(function (keys) {
                    return crypto.subtle.verify(
                        {
                            name: "HMAC"
                        },
                        alice.hmac_key,
                        hexToArrayBuffer(event.event.content.mac),
                        // FIXME: base64-decode and then convert to ArrayBuffer instead
                        stringToArrayBuffer(keys[alice.recallBobs("user_id")][alice.recallBobs("device_id")].getFingerprint())
                    );
                })
                .then(function (isvalid) {
                    if (isvalid) {
                        return alice.client.setDeviceVerified(alice.recallBobs("user_id"), alice.recallBobs("device_id"));
                    } else {
                        throw "Alice's device fails to verify Bob's device key.";
                    }
                })
                .then(function () {
                    alice.log("Alice's device verifies Bob's device key.  Success!");
                    done();
                })
                .catch(error);
            break;
        default:
            error("Alice's device received an unexpected event");
        }
    }

    /** Bob's to_device event handler
     */
    function handleBobEvent(event) {
        console.log("Bob received an event", event); // eslint-disable-line no-console
        switch (event.event.type) {
        case "ca.uhoreg.key_verification.start":
            // from Step 1
            bob.log("Bob's device receives the key verification request");
            bob.log("Bob's device checks that the parameters requested are acceptable"); // TODO:
            bob.log("Bob's device asks Bob if he wants to accept");
            // Note: be careful that this can't be used to DoS Bob's UI.
            // e.g. only prompt if Bob is looking at Alice's profile, or if
            // they're in a room with Alice
            done();
            break;
        case "ca.uhoreg.key_verification.key":
            // from Step 3
            bob.log("Bob's device receives the key");
            crypto.subtle.importKey(
                "raw",
                hexToArrayBuffer(event.event.content.key),
                {
                    name: "ECDH",
                    namedCurve: "P-256"
                },
                true,
                []
            )
                .then(function (key) {
                    bob.rememberAlices("ecdh_publickey", key);
                    done();
                })
                .catch(error);
            break;
        case "ca.uhoreg.key_verification.mac":
            // from Step 7
            bob.log("Bob's device receives the HMAC of Alice's key");
            bob.log("Bob's device checks Alice's device key using the HMAC");
            // FIXME: in a real client, you would probaby use bob.client.getStoredDevice
            bob.client.downloadKeys([bob.recallAlices("user_id")])
                .then(function (keys) {
                    return crypto.subtle.verify(
                        {
                            name: "HMAC"
                        },
                        bob.hmac_key,
                        hexToArrayBuffer(event.event.content.mac),
                        // FIXME: base64-decode and then convert to ArrayBuffer instead
                        stringToArrayBuffer(keys[bob.recallAlices("user_id")][bob.recallAlices("device_id")].getFingerprint())
                    );
                })
                .then(function (isvalid) {
                    if (isvalid) {
                        return bob.client.setDeviceVerified(bob.recallAlices("user_id"), bob.recallAlices("device_id"));
                    } else {
                        throw "Bob's device fails to verify Alice's device key.";
                    }
                })
                .then(function () {
                    bob.log("Bob's device verifies Alice's device key.  Success!");
                    done();
                })
                .catch(error);
            break;
        default:
            error("Bob's device received an unexpected event");
        }
    }

    /** run the next step in the protocol
     */
    function next () {
        btn_next.textContent = "Running...";
        btn_next.disabled = true;

        var logtext = document.createElement("span");
        logtext.innerHTML = steps[curr_step][0];
        var logline = document.createElement("div");
        logline.appendChild(document.createTextNode("Step " + curr_step + ": "));
        logline.setAttribute("class", "description");
        logline.appendChild(logtext);
        addToLog(logline);

        steps[curr_step++][1]();
    }

    btn_next.addEventListener("click", next);
    done();
})();
