#!/bin/sh
set -e
if which yarn > /dev/null; then
    yarn install
else
    npm install
fi
rm -rf public
mkdir -p public
cp index.html e2etest.js style.css public
mkdir public/lib
cp -a lib/*.js public/lib
rm public/lib/olm.js
cp node_modules/olm/olm.js public/lib
mkdir public/images
cp -a node_modules/emojitwo/svg public/images/emojitwo
